Install
=======

Download sources and use setup::

    $ python setup.py install
    or
    $ python setup.py develop


Use
===

Simple usage:

.. code-block:: python

    from draco_stem.draco import DracoMesh



Acknowledgments
===============
