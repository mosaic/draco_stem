# DRACO-STEM

[![Documentation Status](https://readthedocs.org/projects/draco-stem/badge/?version=latest)](https://draco-stem.readthedocs.io/en/latest/?badge=latest)

<image src=https://gitlab.inria.fr/mosaic/draco_stem/-/avatar width=200>


## Generating high-quality meshes of cell tissue from 3D segmented images



**Dual Reconstruction by Adjacency Complex Optimization (DRACO)**

**SAM Tissue Enhanced Mesh (STEM)**

### Authors:
* Guillaume Cerutti (guillaume.cerutti@inria.fr)


### Institutes:
* [<image src=https://www.inria.fr/extension/site_inria/design/site_inria/images/logos/logo_INRIA.png width=100>](http://www.inria.fr)


### License: 
* `Cecill-C`

### Description

Python library for the reconstruction of 3D meshes from segmented shoot apical meristem (SAM) tissue images.

### Installation

```conda install -c mosaic draco-stem```


