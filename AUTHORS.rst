=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Guillaume Cerutti, <guillaume.cerutti@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Guillaume Cerutti <guillaume.cerutti@inria.fr>
* jlegrand62 <jlegrand62@gmail.com>
* GACON Florian <florian.gacon@inria.fr>
* Frederic Boudon <frederic.boudon@cirad.fr>
* LEGRAND Jonathan <jonathan.legrand@inria.fr>
* CERUTTI Guillaume <guillaume.cerutti@inria.fr>

.. #}
