# -*- coding: utf-8 -*-
# -*- python -*-
#
#       DRACO-STEM
#       Dual Reconstruction by Adjacency Complex Optimization
#       SAM Tissue Enhanced Mesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       Mosaic: http://gitlab.inria.fr/mosaic
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.utils.evaluation_tools import jaccard_index

from draco_stem.draco.draco2d import draco_topomesh_2d, cell_vertex_extraction_2d, topomesh_cell_vertex_extraction_2d

from draco_stem.example_image import sphere_tissue_image


class TestDraco2D(unittest.TestCase):
    '''
    Tests the draco_stem.draco.draco2d module.
    '''

    def setUp(self):
        n_points = 12
        np.random.seed(100)
        self.img = sphere_tissue_image(size=100,n_points=n_points)[50]

        self.labels = np.unique(self.img[self.img!=1])
        self.n_cells = len(self.labels)

        self.image_cell_vertex = cell_vertex_extraction_2d(self.img)

    def tearDown(self):
        self.img = None

    def test_draco2d(self):
        topomesh = draco_topomesh_2d(self.img)
        assert topomesh.nb_wisps(3) == self.n_cells

        mesh_cell_vertex = topomesh_cell_vertex_extraction_2d(topomesh)
        vertex_overlap = jaccard_index(np.sort(list(self.image_cell_vertex.keys())),np.sort(list(mesh_cell_vertex.keys())))
        assert vertex_overlap == 1

    def test_draco2d_subdivision(self):
        topomesh = draco_topomesh_2d(self.img,subdivide_edges=True,target_edge_length=5.)

        assert np.isclose(topomesh.wisp_property('length',1).values().mean(),5,1)


