# -*- coding: utf-8 -*-
# -*- python -*-
#
#       DRACO-STEM
#       Dual Reconstruction by Adjacency Complex Optimization
#       SAM Tissue Enhanced Mesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       Mosaic: http://gitlab.inria.fr/mosaic
#
###############################################################################

import unittest

import numpy as np
from scipy import ndimage as nd

from cellcomplex.utils import array_dict

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.utils.evaluation_tools import jaccard_index

from draco_stem.grifone import grifone_topomesh

from draco_stem.example_image import sphere_tissue_image


class TestGrifoneTopomesh(unittest.TestCase):

    def setUp(self):
        self.n_points = 12
        np.random.seed(100)
        self.seg_img = sphere_tissue_image(size=100, n_layers=1, n_points=self.n_points)

    def tearDown(self):
        pass

    def test_grifone_2d(self):
        element_topomesh = grifone_topomesh(self.seg_img, dimension=2, fuse_vertices=True)

        assert element_topomesh.nb_wisps(3) == self.n_points

    def test_grifone_3d(self):
        element_topomesh = grifone_topomesh(self.seg_img, dimension=3, fuse_vertices=True)

        assert element_topomesh.nb_wisps(3) == self.n_points+1

