import logging

from itertools import permutations
from time import time as current_time
from copy import deepcopy

import numpy as np
import scipy.ndimage as nd

from timagetk.components import LabelledImage
from timagetk.plugins.resampling import isometric_resampling

from tissue_analysis.tissue_analysis import TissueImage
from tissue_analysis.array_tools import find_geometric_median

from cellcomplex import PropertyTopomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import clean_topomesh
from cellcomplex.property_topomesh.composition import fuse_topomesh_identical_vertices
from cellcomplex.property_topomesh.topological_operations import topomesh_remove_interface_vertex, topomesh_remove_boundary_vertex, topomesh_remove_interface_edge


def segmentation_topological_element_cell_topomesh(seg_img, dimension=2, use_medians=True, polygonal=False, fuse_vertices=True, resampling_voxelsize=None):

    if resampling_voxelsize is not None:
        if not isinstance(seg_img, LabelledImage):
            seg_img = LabelledImage(seg_img, no_label_id=0, voxelsize=seg_img.voxelsize)
        seg_img = isometric_resampling(seg_img, method=resampling_voxelsize, option='label')
    else:
        resampling_voxelsize = np.max(seg_img.voxelsize)

    if not isinstance(seg_img,TissueImage):
        seg_img = TissueImage(seg_img, background=1, no_label_id=0, voxelsize=seg_img.voxelsize)

    logging.info("  --> Extracting image topological elements")
    start_time = current_time()
    elements_coord = seg_img.topological_elements(element_order=[2, 1, 0])
    for d in [2, 1, 0]:
        for k in elements_coord[d].keys():
            elements_coord[d][k] *= np.array(seg_img.voxelsize)
    logging.info("  <-- Extracting topological elements ["+str(current_time()-start_time)+"s ]")

    logging.info("  --> Linking elements in the triangle mesh")
    start_time = current_time()

    element_topomesh = PropertyTopomesh(3)

    element_vertex_ids = {}
    vertex_dimension = {}
    vertex_position = {}
    face_labels = {}
    cell_labels = {}
    cell_centers = {}

    if dimension == 2:
        for k in elements_coord[2].keys():
            if k[0]==1:
                vid = element_topomesh.add_wisp(0)
                element_vertex_ids[k[1:]] = vid
                vertex_dimension[vid] = 2
                if use_medians:
                    vertex_position[vid] = elements_coord[2][k][find_geometric_median(elements_coord[2][k])]
                else:
                    vertex_position[vid] = np.mean(elements_coord[2][k],axis=0)
                cell_centers[k[1]] = vertex_position[vid]

        cell_edge_ids = {}
        for k in elements_coord[1].keys():
            if k[0] == 1:
                vid = element_topomesh.add_wisp(0)
                element_vertex_ids[k[1:]] = vid
                vertex_dimension[vid] = 1
                if use_medians:
                    vertex_position[vid] = elements_coord[1][k][find_geometric_median(elements_coord[1][k])]
                else:
                    vertex_position[vid] = np.mean(elements_coord[1][k],axis=0)
                for c in k[1:]:
                    if tuple([c]) in element_vertex_ids:
                        cid = element_vertex_ids[(c,)]
                        eid = element_topomesh.add_wisp(1)
                        cell_edge_ids[(c, k[1:])] = eid
                        element_topomesh.link(1, eid, vid)
                        element_topomesh.link(1, eid, cid)

        for k in elements_coord[0].keys():
            if k[0] == 1:
                vid = element_topomesh.add_wisp(0)
                element_vertex_ids[k[1:]] = vid
                vertex_dimension[vid] = 0
                if use_medians:
                    vertex_position[vid] = elements_coord[0][k][find_geometric_median(elements_coord[0][k])]
                else:
                    vertex_position[vid] = np.mean(elements_coord[0][k],axis=0)

                cell_eids = {}
                for c in k[1:]:
                    if tuple([c]) in element_vertex_ids:
                        c_pid = element_vertex_ids[(c,)]
                        eid = element_topomesh.add_wisp(1)
                        cell_eids[c] = eid
                        element_topomesh.link(1, eid, vid)
                        element_topomesh.link(1, eid, c_pid)

                related_edges = np.unique(np.sort(np.array(list(permutations(k[1:])))[:, :2]), axis=0)
                for e in related_edges:
                    if tuple(e) in element_vertex_ids:
                        e_pid = element_vertex_ids[tuple(e)]
                        eid = element_topomesh.add_wisp(1)
                        element_topomesh.link(1, eid, vid)
                        element_topomesh.link(1, eid, e_pid)

                        for c in e:
                            if tuple([c]) in element_vertex_ids:
                                fid = element_topomesh.add_wisp(2)
                                face_labels[fid] = c % 256
                                element_topomesh.link(2, fid, eid)
                                element_topomesh.link(2, fid, cell_eids[c])
                                element_topomesh.link(2, fid, cell_edge_ids[(c, tuple(e))])

                                if not c in element_topomesh.wisps(3):
                                    element_topomesh.add_wisp(3, c)
                                    cell_labels[c] = c % 256
                                element_topomesh.link(3, c, fid)
    elif dimension == 3:
        cell_image_centers = dict(zip(seg_img.labels(), nd.center_of_mass(np.ones_like(seg_img), seg_img, index=seg_img.labels()) * np.array(seg_img.voxelsize)))
                
        for k in elements_coord[2].keys():
            vid = element_topomesh.add_wisp(0)
            element_vertex_ids[k] = vid
            vertex_dimension[vid] = 2
            if use_medians:
                vertex_position[vid] = elements_coord[2][k][find_geometric_median(elements_coord[2][k])]
            else:
                vertex_position[vid] = np.mean(elements_coord[2][k],axis=0)

        wall_edge_ids = {}
        for k in elements_coord[1].keys():
            vid = element_topomesh.add_wisp(0)
            element_vertex_ids[k] = vid
            vertex_dimension[vid] = 1
            if use_medians:
                vertex_position[vid] = elements_coord[1][k][find_geometric_median(elements_coord[1][k])]
            else:
                vertex_position[vid] = np.mean(elements_coord[1][k], axis=0)

            related_walls = np.unique(np.sort(np.array(list(permutations(k)))[:, :2]), axis=0)
            for w in related_walls:
                if tuple(w) in element_vertex_ids:
                    wid = element_vertex_ids[tuple(w)]
                    eid = element_topomesh.add_wisp(1)
                    wall_edge_ids[(tuple(w), k)] = eid
                    element_topomesh.link(1, eid, vid)
                    element_topomesh.link(1, eid, wid)

        for k in elements_coord[0].keys():
            vid = element_topomesh.add_wisp(0)
            element_vertex_ids[k] = vid
            vertex_dimension[vid] = 0
            if use_medians:
                vertex_position[vid] = elements_coord[0][k][find_geometric_median(elements_coord[0][k])]
            else:
                vertex_position[vid] = np.mean(elements_coord[0][k],axis=0)

            wall_eids = {}
            related_walls = np.unique(np.sort(np.array(list(permutations(k)))[:, :2]), axis=0)
            for w in related_walls:
                if tuple(w) in element_vertex_ids:
                    c_pid = element_vertex_ids[tuple(w)]
                    eid = element_topomesh.add_wisp(1)
                    wall_eids[tuple(w)] = eid
                    element_topomesh.link(1, eid, vid)
                    element_topomesh.link(1, eid, c_pid)

            related_edges = np.unique(np.sort(np.array(list(permutations(k)))[:, :3]), axis=0)
            for e in related_edges:
                if tuple(e) in element_vertex_ids:
                    e_pid = element_vertex_ids[tuple(e)]
                    eid = element_topomesh.add_wisp(1)
                    element_topomesh.link(1, eid, vid)
                    element_topomesh.link(1, eid, e_pid)

                    related_walls = np.unique(np.sort(np.array(list(permutations(e)))[:, :2]), axis=0)
                    for w in related_walls:
                        if tuple(w) in element_vertex_ids:
                            fid = element_topomesh.add_wisp(2)
                            face_labels[fid] = w[1] % 256
                            element_topomesh.link(2, fid, eid)
                            element_topomesh.link(2, fid, wall_eids[tuple(w)])
                            element_topomesh.link(2, fid, wall_edge_ids[(tuple(w), tuple(e))])

                            for c in w:
                                if c!=1:
                                    if not c in element_topomesh.wisps(3):
                                        element_topomesh.add_wisp(3, c)
                                        cell_labels[c] = c % 256
                                    element_topomesh.link(3, c, fid)
        
        cell_centers = {c:cell_image_centers[c] for c in element_topomesh.wisps(3)}

    element_topomesh.update_wisp_property('barycenter', 0, vertex_position)
    element_topomesh.update_wisp_property('dimension', 0, vertex_dimension)
    element_topomesh.update_wisp_property('label', 2, face_labels)
    element_topomesh.update_wisp_property('label', 3, cell_labels)
    element_topomesh.update_wisp_property('center', 3, cell_centers)

    logging.info("  <-- Linking elements in the triangle mesh ["+str(current_time() - start_time)+"s ]")

    if fuse_vertices:
        element_topomesh_fuse_vertices(element_topomesh,distance_threshold=2 * resampling_voxelsize)

    if polygonal:
        return polygonal_element_topomesh(element_topomesh, dimension=dimension)

    return element_topomesh


def element_topomesh_fuse_vertices(element_topomesh, distance_threshold=0.1):
    logging.info("  --> Fusing un-merged atypical cell junctions")
    start_time = current_time()

    cell_vertex_vertices = np.array([v for v in element_topomesh.wisps(0) if element_topomesh.wisp_property('dimension', 0)[v] == 0])
    cell_vertex_points = element_topomesh.wisp_property('barycenter', 0).values(cell_vertex_vertices)

    cell_vertex_distances = np.linalg.norm(cell_vertex_points[:, np.newaxis] - cell_vertex_points[np.newaxis], axis=-1)
    cell_vertex_distances += 2 * np.eye(len(cell_vertex_vertices)) * distance_threshold

    close_points = np.where(cell_vertex_distances < distance_threshold)
    close_vertices = np.transpose([cell_vertex_vertices[close_points[k]] for k in [0, 1]])

    vertices_to_fuse = {v: {v} for v in np.unique(close_vertices)}
    for v in close_vertices:
        if np.all([np.any([element_topomesh.nb_regions(1, eid) == 1 for eid in element_topomesh.regions(0, vid)]) for vid in v]):
            v1, v2 = np.sort(v)
            vertices_to_fuse[v1] |= vertices_to_fuse[v2]
    redundant_vertices_to_fuse = [v for v in vertices_to_fuse.keys() if np.any([vertices_to_fuse[v] & f == vertices_to_fuse[v] for f in vertices_to_fuse.values() if f != vertices_to_fuse[v]])]
    for v in redundant_vertices_to_fuse:
        del vertices_to_fuse[v]

    for vids in vertices_to_fuse.values():
        center = element_topomesh.wisp_property('barycenter', 0).values(list(vids)).mean(axis=0)
        for vid in vids:
            element_topomesh.wisp_property('barycenter', 0)[vid] = center

    fuse_topomesh_identical_vertices(element_topomesh)
    logging.info("  <-- Fusing un-merged atypical cell junctions ["+str(current_time() - start_time)+"s ]")


def polygonal_element_topomesh(input_element_topomesh, dimension=2):
    element_topomesh = deepcopy(input_element_topomesh)
    
    if dimension == 2:
        compute_topomesh_property(element_topomesh, "contour", 0)
        contour_face_vertices = [v for v in element_topomesh.wisps(0) if element_topomesh.wisp_property('dimension', 0)[v] == 2 and element_topomesh.wisp_property('contour', 0)[v]]
        for v in contour_face_vertices:
            vertex_face_edges = [e for e in element_topomesh.regions(0, v) if not element_topomesh.wisp_property('contour', 1)[e]]
            for e in vertex_face_edges:
                topomesh_remove_interface_edge(element_topomesh, e)
            element_topomesh.wisp_property('dimension', 0)[v] = 0
        element_topomesh = clean_topomesh(element_topomesh, clean_properties=True)
    
        face_vertices = [v for v in element_topomesh.wisps(0) if element_topomesh.wisp_property('dimension', 0)[v] == 2]
        for v in face_vertices:
            topomesh_remove_interface_vertex(element_topomesh, v)
        element_topomesh = clean_topomesh(element_topomesh, clean_properties=True)
    
        edge_vertices = [v for v in element_topomesh.wisps(0) if element_topomesh.wisp_property('dimension', 0)[v] == 1]
        for v in edge_vertices:
            topomesh_remove_boundary_vertex(element_topomesh, v)
        element_topomesh = clean_topomesh(element_topomesh, clean_properties=True)
        
    return element_topomesh
