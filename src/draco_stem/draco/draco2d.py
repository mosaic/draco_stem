import logging

import numpy as np
import scipy.ndimage as nd
from scipy.cluster.vq import vq

import matplotlib.pyplot as plt
from imageio import imread

from cellcomplex.utils import array_dict

from cellcomplex.property_topomesh.creation import triangle_topomesh, edge_topomesh, dual_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import clean_topomesh_properties
from cellcomplex.property_topomesh.optimization import property_topomesh_edge_flip_optimization, property_topomesh_vertices_deformation, property_topomesh_edge_split_optimization

from cellcomplex.property_topomesh.utils.delaunay_tools import delaunay_triangulation

plt.ion()


def cell_vertex_extraction_2d(img):

    neighborhood_img = []
    for x in np.arange(-1, 1):
        for y in np.arange(-1, 1):
            neighborhood_img.append(img[1 + x:img.shape[0] + x, 1 + y:img.shape[1] + y])
    neighborhood_img = np.sort(np.transpose(neighborhood_img, (1, 2, 0))).reshape((np.array(img.shape) - 1).prod(), 4)

    pixel_coords = np.transpose(np.mgrid[0:img.shape[0] - 1, 0:img.shape[1] - 1], (1, 2, 0)).reshape(((np.array(img.shape) - 1).prod(), 2)) + 0.5

    non_flat = np.logical_not(np.all(neighborhood_img == np.tile(neighborhood_img[:, :1], (1, 4)), axis=1))
    neighborhood_img = neighborhood_img[non_flat]
    pixel_coords = pixel_coords[non_flat]

    neighborhoods = np.array(list(map(np.unique, neighborhood_img)))
    neighborhood_size = np.array(list(map(len, neighborhoods)))
    neighborhoods = np.array(neighborhoods)

    vertex_points = pixel_coords[neighborhood_size == 3][:, ::-1]
    vertex_cells = np.array([p for p in neighborhoods[neighborhood_size == 3]], int)
    if (neighborhood_size == 4).sum() > 0:
        clique_vertex_points = np.concatenate([(p, p) for p in pixel_coords[neighborhood_size == 4]])[:, ::-1]
        clique_vertex_cells = np.concatenate([[p[:3], np.concatenate([[p[0]], p[2:]])] for p in neighborhoods[neighborhood_size == 4]]).astype(int)
        vertex_points = np.concatenate([vertex_points, clique_vertex_points])
        vertex_cells = np.concatenate([vertex_cells, clique_vertex_cells])

    unique_cell_vertices = np.unique(vertex_cells, axis=0)
    vertex_matching = vq(vertex_cells, unique_cell_vertices)[0]
    image_cell_vertex = dict(list(zip([tuple(v) for v in unique_cell_vertices], [list(np.mean(vertex_points[vertex_matching == v], axis=0)) + [0] for v, _ in enumerate(unique_cell_vertices)])))

    return image_cell_vertex


def topomesh_cell_vertex_extraction_2d(topomesh,background_label=1):

    compute_topomesh_property(topomesh, 'cells', degree=0)
    vertex_cell_neighbours = topomesh.wisp_property('cells', degree=0)

    topomesh.update_wisp_property('boundary', 1, dict(list(zip(topomesh.wisps(1), [topomesh.nb_regions(1, e) < 2 for e in topomesh.wisps(1)]))))
    compute_topomesh_property(topomesh, 'edges', 0)
    topomesh.update_wisp_property('boundary', 0,
                                  dict(list(zip(topomesh.wisps(0), list(map(np.any, topomesh.wisp_property('boundary', 1).values(topomesh.wisp_property('edges', 0).values(list(topomesh.wisps(0))))))))))
    boundary_vertices = np.array(list(topomesh.wisps(0)))[np.where(topomesh.wisp_property('boundary', degree=0).values())]

    mesh_cell_vertex = {}
    for v in topomesh.wisps(0):
        if len(vertex_cell_neighbours[v]) == 3:
            vertex_cell_labels = tuple(list(np.sort([c for c in vertex_cell_neighbours[v]])))
            mesh_cell_vertex[vertex_cell_labels] = v
        if (len(vertex_cell_neighbours[v]) == 2) and (v in boundary_vertices):
            vertex_cell_labels = (background_label,) + tuple(list(np.sort([c for c in vertex_cell_neighbours[v]])))
            mesh_cell_vertex[vertex_cell_labels] = v

    return mesh_cell_vertex


def draco_topomesh_2d(img=None, image_filename=None, background_label=1, contour_size=None, simulated_annealing=False, cell_vertex_motion=True, smoothing=True, subdivide_edges=False, target_edge_length=2.):
    """

    :param img:
    :param image_filename:
    :param background_label:
    :return:
    """

    assert (img is not None) or (image_filename is not None)

    if img is None:
        img = imread(image_filename)
    if img.ndim == 3:
        img = np.mean(img, axis=2).astype(img.dtype)

    logging.info("--> Extracting cell centers")

    labels = np.unique(img[img != background_label])
    cell_points = np.array(nd.center_of_mass(np.ones_like(img), img, labels))[:, ::-1]
    cell_centers = dict(list(zip(labels, np.concatenate([cell_points, np.zeros_like(cell_points[:, :1])], axis=1))))
    cell_centers = array_dict(cell_centers)
    n_cells = len(labels)

    logging.info("--> Computing image cell adjacencies")

    image_cell_vertex = cell_vertex_extraction_2d(img)

    edge_cells = np.concatenate(np.array(list(image_cell_vertex.keys()))[:, [[0, 1], [0, 2], [1, 2]]])
    # edge_points = pixel_coords[neighborhood_size == 2]
    # edge_cells = np.array([p for p in neighborhoods[neighborhood_size == 2]], int)
    unique_cell_edges = np.unique(edge_cells, axis=0)
    image_cell_edges = unique_cell_edges[np.logical_not(np.any(unique_cell_edges == background_label, axis=1))]

    image_topomesh = edge_topomesh(image_cell_edges, cell_centers)
    image_topomesh.add_wisp(2, 0)
    for e in image_topomesh.wisps(1):
        image_topomesh.link(2, 0, e)

    logging.info("--> Delineating image contour")

    radius = np.sqrt((img != background_label).sum() / n_cells) / np.pi
    morpho_iterations = int(2. * radius)
    padding = morpho_iterations + 1
    padded_img = np.pad(img, (padding, padding), mode='constant', constant_values=background_label)

    if contour_size is None:
        # contour_size = int((np.pi*radius)/target_edge_length)
        contour_size = 2 * n_cells

    figure = plt.figure()
    shape_img = nd.binary_dilation(padded_img != background_label, iterations=morpho_iterations)
    # contour = figure.gca().contour(shape_img, [0.5])
    contour = figure.gca().contour(shape_img, [0.5])
    contour_points = np.concatenate([path.to_polygons()[0] for path in contour.collections[0].get_paths()])
    contour_points -= padding
    # contour_points = contour_points[:,::-1]
    # contour_points = np.array([img.shape[0],img.shape[1]]) + np.array([-1,-1])*contour_points
    plt.close(figure)
    contour_indices = np.linspace(0, len(contour_points - 1), contour_size + 1)[:-1].astype(int)
    contour_points = contour_points[contour_indices]

    logging.info("--> Computing Delaunay triangulation")

    all_points = np.transpose([np.concatenate([cell_points[:, 0], contour_points[:, 0]]), np.concatenate([cell_points[:, 1], contour_points[:, 1]]), np.zeros(int(np.round(n_cells + contour_size)))])
    all_positions = array_dict(all_points, keys=list(labels) + list(range(labels.max() + 1, labels.max() + contour_size + 1)))
    triangles = all_positions.keys()[delaunay_triangulation(all_positions.values())]

    triangulation_topomesh = triangle_topomesh(triangles, all_positions)

    logging.info("--> Optimizing adjacency complex")

    if simulated_annealing:
        for cycle in range(3):
            property_topomesh_edge_flip_optimization(triangulation_topomesh, image_edges=image_cell_edges, omega_energies={'image': 1.0, 'regularization': 0.1}, iterations=10, simulated_annealing=True)
    else:
        property_topomesh_edge_flip_optimization(triangulation_topomesh, image_edges=image_cell_edges, omega_energies={'image': 1.0}, iterations=20, simulated_annealing=False)

    from cellcomplex.property_topomesh.utils.evaluation_tools import performance_measure

    triangulation_edges = triangulation_topomesh.wisp_property("vertices",1).values()
    cell_edge_recall = performance_measure(np.sort(image_cell_edges), np.sort(triangulation_edges), measure='recall')
    print("  --> Cell Edge Recall = "+str(np.round(cell_edge_recall,2)))

    logging.info("--> Creating dual reconstruction")

    topomesh = dual_topomesh(triangulation_topomesh, 2, vertex_positions='projected_voronoi')

    for c in range(labels.max() + 1, labels.max() + contour_size + 1):
        if c in topomesh.wisps(3):
            topomesh.remove_wisp(3, c)

    faces_to_remove = [f for f in topomesh.wisps(2) if topomesh.nb_regions(2, f) == 0]
    for f in faces_to_remove:
        topomesh.remove_wisp(2, f)

    edges_to_remove = [e for e in topomesh.wisps(1) if topomesh.nb_regions(1, e) == 0]
    for e in edges_to_remove:
        topomesh.remove_wisp(1, e)

    vertices_to_remove = [v for v in topomesh.wisps(0) if topomesh.nb_regions(0, v) == 0]
    for v in vertices_to_remove:
        topomesh.remove_wisp(0, v)

    clean_topomesh_properties(topomesh)
    topomesh.update_wisp_property('cell', 2, dict(list(zip(labels, labels%256))))

    if cell_vertex_motion:
        logging.info("--> Moving cell vertices")

        mesh_cell_vertex = topomesh_cell_vertex_extraction_2d(topomesh,background_label=background_label)

        # cell_vertex_matching = vq(np.sort(array_dict(image_cell_vertex).keys()),np.sort(array_dict(mesh_cell_vertex).keys()))
        cell_vertex_matching = vq(np.sort(np.array(list(image_cell_vertex.keys()))), np.sort(np.array(list(mesh_cell_vertex.keys()))))

        matched_image_index = np.where(cell_vertex_matching[1] == 0)[0]
        matched_mesh_index = cell_vertex_matching[0][np.where(cell_vertex_matching[1] == 0)[0]]

        matched_image_cell_vertex = np.array(list(image_cell_vertex.values()))[matched_image_index]
        matched_keys = np.sort(np.array(list(image_cell_vertex.keys())))[matched_image_index]

        matched_mesh_vertices = np.array(list(mesh_cell_vertex.values()))[cell_vertex_matching[0][np.where(cell_vertex_matching[1] == 0)[0]]]
        matched_keys = np.sort(np.array(list(mesh_cell_vertex.keys())))[matched_mesh_index]

        initial_vertex_positions = topomesh.wisp_property('barycenter', 0).to_dict()

        final_vertex_positions = {}
        fixed_vertex = array_dict(np.array([False for v in topomesh.wisps(0)]), np.array(list(topomesh.wisps(0))))
        for i, v in enumerate(matched_mesh_vertices):
            if not np.isnan(matched_image_cell_vertex[i]).any():
                final_vertex_positions[v] = matched_image_cell_vertex[i]
                # print topomesh.wisp_property('barycenter',0)[v]," -> ",final_vertex_positions[v]
                fixed_vertex[v] = True
        final_vertex_positions = array_dict(final_vertex_positions)
        matched_mesh_vertices = final_vertex_positions.keys()

    iterations = 20

    if smoothing:
        logging.info("--> Smoothing mesh edges")

    for iteration in range(iterations):

        if subdivide_edges:
            property_topomesh_edge_split_optimization(topomesh, maximal_length=4./3.*target_edge_length, split_adjacent_faces=False)

        if smoothing:
            # property_topomesh_vertices_deformation(topomesh,iterations=5,omega_forces={'taubin_smoothing':0.66},sigma_deformation=1.,gaussian_sigma=np.sqrt(np.prod(img.shape)))
            for i in range(5):
                property_topomesh_vertices_deformation(topomesh, iterations=1, omega_forces={'laplacian_smoothing': 0.33}, sigma_deformation=1.)
                property_topomesh_vertices_deformation(topomesh, iterations=1, omega_forces={'laplacian_smoothing': -0.34}, sigma_deformation=1.)
        if cell_vertex_motion:
            for v in matched_mesh_vertices:
                topomesh.wisp_property('barycenter', degree=0)[v] = ((iterations - 1 - iteration) * initial_vertex_positions[v] + (iteration + 1) * final_vertex_positions[v]) / iterations

    return topomesh
